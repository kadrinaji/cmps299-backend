import requests
from bs4 import BeautifulSoup
# multithreading
import concurrent.futures
import requests

def Filter_Crossed(div):
    for span in div.findAll('span', {'class': 'diff_del diff_diff'}):
        span.decompose()
    return div


def Translate(inp):

    url = f"https://translate.systran.net/?input={inp}&source=ar&target=en"

    r = requests.get('http://localhost:9000/render.html', params={'url': url, 'wait': 5})

    soup = BeautifulSoup(r.text, 'html.parser')

    results = soup.findAll('div', {'class': 'alternativesTranslation'})

    # Generic_Model = Filter_Crossed(results[0]).text

    GenericPlus_Model = Filter_Crossed(results[0]).text

    It_Model = Filter_Crossed(results[1]).text

    return {'GenericPlus Model': GenericPlus_Model,
            'IT Model': It_Model}


def Translate_Parallel(inp_list):

    with concurrent.futures.ThreadPoolExecutor() as executor:
        urls = [executor.submit(Translate, inp) for inp in inp_list]
        concurrent.futures.wait(urls)
        return [url.result() for url in urls]

# print(Translate("شو بدك مني"))
# print(Translate("شو اشبك"))