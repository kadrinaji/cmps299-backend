import os, flask, Leb2Eng, json
from flask import request, render_template, jsonify, Response
from werkzeug.utils import secure_filename
import speech_recognition as sr
from googletrans import Translator # Using googletrans==3.1.0a0
from flask_cors import CORS
# Pydub
from pydub import AudioSegment
from pydub.playback import play
from pydub.silence import split_on_silence
# multiprocessing
from multiprocessing import Process

r = sr.Recognizer()

app = flask.Flask(__name__)
app.config['DEBUG'] = True
CORS(app)

translator = Translator()


@app.route('/info', methods=['GET'])
def info():
    return 'Lebanese to MSA (API server) © 2021'

@app.route('/', methods=['GET'])
def home():
    return render_template('upload.html')

@app.route('/api/transcribe', methods=['POST'])
def transcribe():

    f = request.files['file']
    filename = secure_filename(f.filename)
    audio_path = os.path.join('./temp/', filename)
    f.save(audio_path)

    sample = sr.AudioFile(audio_path)

    with sample as source:
        r.adjust_for_ambient_noise(source, duration=0.5)
        audio = r.record(source)

    res = r.recognize_google(audio, language="ar-LB")

    os.remove(audio_path)

    return res

@app.route('/api/translate', methods=['GET'])
def full_translation():

    txt = request.args.get('input')

    if txt == None:
        return Response('<span>You have to enter an input</span>', status=400)

    eng_txt = Leb2Eng.Translate(txt)

    translation = {
        # 'Generic Model': translator.translate(eng_txt['Generic Model'], src='en', dest='ar').text,
        'GenericPlus Model': translator.translate(eng_txt['GenericPlus Model'], src='en', dest='ar').text,
        'IT Model': translator.translate(eng_txt['IT Model'], src='en', dest='ar').text

    }
    
    result = json.dumps([eng_txt, translation], ensure_ascii=False).encode('utf8')

    return Response(result, status=200, mimetype='application/json')



@app.route('/api/leb2eng', methods=['GET'])
def translate2eng():
    txt = request.args.get('input')
    
    if txt == None:

        return Response('<span>You have to enter an input</span>', status=400)
    
    result = Leb2Eng.Translate(txt)

    return jsonify(result)

@app.route('/api/eng2msa', methods=['GET'])
def translate2msa():
    txt = request.args.get('input')
    
    if txt == None:

        return Response('<span>You have to enter an input</span>', status=400)
    
    translation = translator.translate(txt, src='en', dest='ar')
    result = translation.text

    return result

# @app.route('/api/upload', methods=['POST'])
# def upload():
#     f = request.files['file']
#     filename = secure_filename(f.filename)
#     audio_path = os.path.join('./temp/', filename)
#     f.save(audio_path)

#     sample = sr.AudioFile(audio_path)

#     with sample as source:
#         r.adjust_for_ambient_noise(source, duration=0.5)
#         audio = r.record(source)

#     transcription = r.recognize_google(audio, language="ar-LB")

#     os.remove(audio_path)

#     eng_txt = Leb2Eng.Translate(transcription)

#     translation = {
#         # 'Generic Model': translator.translate(eng_txt['Generic Model'], src='en', dest='ar').text,
#         'GenericPlus Model': translator.translate(eng_txt['GenericPlus Model'], src='en', dest='ar').text,
#         'IT Model': translator.translate(eng_txt['IT Model'], src='en', dest='ar').text

#     }


#     result = json.dumps([transcription, eng_txt, translation], ensure_ascii=False).encode('utf8')

#     return Response(result, status=200, mimetype='application/json')

@app.route('/api/upload', methods=['POST'])
def upload():
    f = request.files['file']
    filename = secure_filename(f.filename)
    audio_path = os.path.join('./temp/', filename)
    f.save(audio_path)

    sample = AudioSegment.from_wav(audio_path)

    chunks = split_on_silence(sample, min_silence_len=500, silence_thresh=-42, keep_silence=True)

    for i, chunk in enumerate(chunks):
        filename = f'chunk{i}.wav'
        chunk.export(f'./temp/{filename}', format='wav')

    transcriptions = []

    for i in range(len(chunks)):
        chunk = sr.AudioFile(f'./temp/chunk{i}.wav')
        with chunk as source:
            r.adjust_for_ambient_noise(chunk, duration=0.5)
            audio = r.record(chunk)
            transcriptions.append(r.recognize_google(audio, language="ar-LB"))

    os.remove(audio_path)

    for i in range(len(chunks)):
        os.remove(f'./temp/chunk{i}.wav')


    eng_txts = Leb2Eng.Translate_Parallel(transcriptions)

    transcription = ' '.join(transcriptions)

    eng_txt = {'GenericPlus Model': '', 'IT Model': ''}

    translation = {'GenericPlus Model': '', 'IT Model': ''}

    for txt in eng_txts:
        eng_txt['GenericPlus Model'] = eng_txt['GenericPlus Model'] + " " + txt['GenericPlus Model']
        eng_txt['IT Model'] = eng_txt['IT Model'] + " " + txt['IT Model']
        translation['GenericPlus Model'] = translation['GenericPlus Model'] + " " + \
        translator.translate(txt['GenericPlus Model'], src='en', dest='ar').text
        translation['IT Model'] = translation['IT Model'] + " " + \
        translator.translate(txt['IT Model'], src='en', dest='ar').text

    result = json.dumps([transcription, eng_txt, translation], ensure_ascii=False).encode('utf8')

    return Response(result, status=200, mimetype='application/json')

if __name__ == '__main__':
    app.run()